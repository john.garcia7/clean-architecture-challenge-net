﻿using Pragma.Clients.API.ApiModels;

namespace Pragma.Clients.API.Interfaces.ApiOrchestrators
{
    public interface IGetClientOrchestrator
    {
        Task<ClientDto> Get(int id);
    }
}