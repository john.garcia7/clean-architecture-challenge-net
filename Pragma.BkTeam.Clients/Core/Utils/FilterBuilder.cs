﻿using Pragma.Clients.Core.Enums;
using Pragma.Clients.Core.Exceptions;
using Pragma.Clients.Core.Interfaces;
using Pragma.Clients.Core.PocoEntities;
using System.Linq.Expressions;

namespace Pragma.Clients.Core.Utils
{
    public class FilterBuilder : IFilterBuilder
    {
        #region Fields

        private readonly IExpressionCombiner _expressionCombiner;

        #endregion

        #region Constructor

        public FilterBuilder(
            IExpressionCombiner expressionCombiner)
        {
            _expressionCombiner = expressionCombiner;
        }

        #endregion

        #region Public Methods

        public Expression<Func<Client, bool>> Build(SearchFilter searchFilter)
        {
            if (searchFilter == null)
            {
                return null;
            }

            IdentificationType identificationTypeFilter;
            bool successIdTypeConv = Enum.TryParse(searchFilter.IdType, out identificationTypeFilter);
            if (!string.IsNullOrWhiteSpace(searchFilter.IdType) && !successIdTypeConv)
            {
                throw new InvalidIdentificationTypeException(Resources.Messages.InvalidIdentificationTypeExceptionMsg);
            }

            Expression<Func<Client, bool>> ageFilter = searchFilter.Age > 0 ? c => c.Age >= searchFilter.Age : null;
            Expression<Func<Client, bool>> idTypeFilter = !string.IsNullOrWhiteSpace(searchFilter.IdType) ? c => c.IdType.Equals(identificationTypeFilter) : null;
            Expression<Func<Client, bool>> idNumberFilter = !string.IsNullOrWhiteSpace(searchFilter.IdNumber) ? c => c.IdNumber.Equals(searchFilter.IdNumber) : null;

            Expression<Func<Client, bool>> searchExpression = null;

            searchExpression = _expressionCombiner.Combine(searchExpression, ageFilter);
            searchExpression = _expressionCombiner.Combine(searchExpression, idTypeFilter);
            searchExpression = _expressionCombiner.Combine(searchExpression, idNumberFilter);

            return searchExpression;
        }

        #endregion
    }
}
