﻿using MongoDB.Bson;
using Pragma.Clients.Infrastructure.Interfaces.Data.Utils;
using System.Text.Json;

namespace Pragma.Clients.Infrastructure.Data.Utils
{
    public class BsonDocumentSerializer<T> : IBsonDocumentSerializer<T> where T : class
    {
        #region Public Methods

        public BsonDocument Serialize(T objectToSerialize)
        {
            if (objectToSerialize == null)
            {
                return null;
            }

            string json = JsonSerializer.Serialize(objectToSerialize);

            return BsonDocument.Parse(json);
        }

        #endregion
    }
}
