using Moq;
using Pragma.Clients.Core.Enums;
using Pragma.Clients.Core.Exceptions;
using Pragma.Clients.Core.Interfaces;
using Pragma.Clients.Core.PocoEntities;
using Pragma.Clients.Core.Services;
using System.Linq.Expressions;

namespace Pragma.BkTeam.Clients.UnitTests.Core.Services
{
    [TestClass]
    public class AddClientServiceTest
    {
        #region Fields
        
        private IAddClientService _addClientService;
        private Mock<IRepository<Client>> _repository;

        #endregion

        #region Initialization and Clean Up

        [TestInitialize]
        public void Initialize()
        {
            _repository = new Mock<IRepository<Client>>();

            _addClientService = new AddClientService(_repository.Object);
        } 

        #endregion

        #region Test Methods

        [TestMethod]
        public void AddClientTest()
        {
            #region Arrange
            
            Client client = new Client() 
            { 
                FirstName = "Sharon",
                LastName = "Stone",
                CityOfBirth = "New York",
                IdType = IdentificationType.DNI,
                IdNumber = "648374623832"
            };

            #endregion

            #region Act
            
            _addClientService.Add(client);

            #endregion

            #region Assert
            
            _repository.Verify(x => x.Insert(client), Times.Once());

            #endregion
        }

        [TestMethod]
        public async Task DuplicatedClientTest()
        {
            #region Arrange
            
            Client existingClient = new Client()
            {
                FirstName = "Sharon",
                LastName = "Stone",
                CityOfBirth = "New York",
                IdType = IdentificationType.DNI,
                IdNumber = "648374623832"
            };

            Client newClient = new Client()
            {
                FirstName = "Demi",
                LastName = "Moore",
                CityOfBirth = "Miami",
                IdType = IdentificationType.DNI,
                IdNumber = "648374623832"
            };

            _repository.Setup(x =>
                x.GetFirst(It.IsAny<Expression<Func<Client, bool>>>())).Returns(Task.FromResult(existingClient));

            #endregion

            #region Act and Assert

            await Assert.ThrowsExceptionAsync<DuplicatedItemException>(() => _addClientService.Add(newClient));
            _repository.Verify(x => x.Insert(newClient), Times.Never());

            #endregion
        }

        #endregion
    }
}