﻿using Pragma.Clients.Core.PocoEntities;

namespace Pragma.Clients.Core.Interfaces
{
    public interface IAddClientService
    {
        Task<Client> Add(Client client);
    }
}