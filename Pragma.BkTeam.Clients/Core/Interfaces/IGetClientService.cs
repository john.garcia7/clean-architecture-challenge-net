﻿using Pragma.Clients.Core.PocoEntities;

namespace Pragma.Clients.Core.Interfaces
{
    public interface IGetClientService
    {
        Task<Client> Get(int id);
    }
}