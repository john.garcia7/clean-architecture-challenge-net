﻿namespace Pragma.Clients.Core.PocoEntities
{
    public class GenericResultsPage<T> where T : class
    {
        #region Constructor
        
        public GenericResultsPage()
        {
            PageInfo = new PageInfo();
        } 

        #endregion

        #region Properties

        public PageInfo PageInfo { get; set; }

        public int TotalResults { get; set; }

        public int TotalPages { get; set; }

        public List<T> Results { get; set; } 

        #endregion
    }
}
