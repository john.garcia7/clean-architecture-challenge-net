﻿using System.Linq.Expressions;

namespace Pragma.Clients.Core.Extensions
{
    #region Extensions for .Net Expressions Concatenation

    internal static class PredicateExtensions
    {
        #region Operation Extensions
        
        public static Expression<Func<T, bool>> And<T>(
            this Expression<Func<T, bool>> firstExpression,
            Expression<Func<T, bool>> secondExpression)
        {
            var parameter = Expression.Parameter(typeof(T));

            var leftVisitor = new ReplaceExpressionVisitor(firstExpression.Parameters[0], parameter);
            var left = leftVisitor.Visit(firstExpression.Body);

            var rightVisitor = new ReplaceExpressionVisitor(secondExpression.Parameters[0], parameter);
            var right = rightVisitor.Visit(secondExpression.Body);

            return Expression.Lambda<Func<T, bool>>(
                Expression.And(left, right), parameter);
        } 

        #endregion
    }

    internal class ReplaceExpressionVisitor : ExpressionVisitor
    {
        #region Fields
        
        private readonly Expression _oldExpression;
        private readonly Expression _newExpression;

        #endregion

        #region Constructor
        
        public ReplaceExpressionVisitor(
            Expression oldExpression,
            Expression newExpression)
        {
            _oldExpression = oldExpression;
            _newExpression = newExpression;
        }

        #endregion

        #region Expression Override
        
        public override Expression Visit(Expression node)
        {
            if (node == _oldExpression)
                return _newExpression;

            return base.Visit(node);
        } 

        #endregion
    } 

    #endregion
}
