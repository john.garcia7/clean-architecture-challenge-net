﻿using Pragma.Clients.Core.Exceptions;
using Pragma.Clients.Core.Interfaces;
using Pragma.Clients.Core.PocoEntities;

namespace Pragma.Clients.Core.Services
{
    public class UpdateClientService : IUpdateClientService
    {
        #region Fields

        private readonly IRepository<Client> _repository;

        #endregion

        #region Constructor

        public UpdateClientService(
            IRepository<Client> repository)
        {
            _repository = repository;
        }

        #endregion

        #region Public Method

        public async Task<Client> Update(Client client)
        {
            bool itemExists = await _repository.GetById(client.Id) != null;

            if (!itemExists)
            {
                throw new ItemNotFoundException(string.Format(Resources.Messages.ItemNotFoundExceptionMsg, client.Id));
            }

            bool clientExists = await _repository.GetFirst(dbClient =>
                dbClient.IdType == client.IdType && dbClient.IdNumber == client.IdNumber && dbClient.Id != client.Id) != null;

            if (clientExists)
            {
                throw new DuplicatedItemException(
                    string.Format(Resources.Messages.DuplicatedItemExceptionMsg, Enum.GetName(client.IdType), client.IdNumber));
            }

            return await _repository.Update(client);
        }

        #endregion
    }
}
