﻿namespace Pragma.Clients.Core.Enums
{
    public enum IdentificationType
    {
        CC,
        NIT,
        DNI
    }
}
