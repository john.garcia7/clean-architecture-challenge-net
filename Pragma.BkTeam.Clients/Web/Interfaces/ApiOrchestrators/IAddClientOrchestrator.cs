﻿using Pragma.Clients.API.ApiModels;

namespace Pragma.Clients.API.Interfaces.ApiOrchestrators
{
    public interface IAddClientOrchestrator
    {
        Task<ClientDto> Add(ClientDto client);
    }
}