﻿using Microsoft.EntityFrameworkCore;
using Pragma.Clients.Core.Interfaces;
using Pragma.Clients.Core.PocoEntities;
using System.Linq.Expressions;

namespace Pragma.Clients.Infrastructure.Data
{
    public class GenericRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        #region Fields

        private readonly ClientDbContext _clientDbContext;

        #endregion

        #region Constructor

        public GenericRepository(ClientDbContext clientDbContext)
        {
            _clientDbContext = clientDbContext;
        }

        #endregion

        #region Public Methods

        public async Task<List<TEntity>> GetAll(Expression<Func<TEntity, bool>> searchExpression = null)
        {
            try
            {
                if (searchExpression == null)
                {
                    return await _clientDbContext.Set<TEntity>().ToListAsync();
                }
                else
                {
                    return await _clientDbContext.Set<TEntity>().Where(searchExpression).ToListAsync();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<GenericResultsPage<TEntity>> GetAllPaged(int page, int pageSize, Expression<Func<TEntity, bool>> searchExpression = null)
        {
            GenericResultsPage<TEntity> resultsPage = new GenericResultsPage<TEntity>();

            if (searchExpression == null)
            {               
                resultsPage.Results = await _clientDbContext.Set<TEntity>().Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();
                resultsPage.TotalResults = await _clientDbContext.Set<TEntity>().CountAsync();
            }
            else
            {
                resultsPage.Results = await _clientDbContext.Set<TEntity>().Where(searchExpression).Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();
                resultsPage.TotalResults = await _clientDbContext.Set<TEntity>().Where(searchExpression).CountAsync();
            }

            resultsPage.PageInfo.Page = page;
            resultsPage.PageInfo.PageSize = pageSize;
            resultsPage.TotalPages = Convert.ToInt32(Math.Ceiling((Convert.ToDouble(resultsPage.TotalResults) / Convert.ToDouble(pageSize))));

            return resultsPage;
        }

        public async Task<TEntity> GetById(int id)
        {
            try
            {
                return await _clientDbContext.Set<TEntity>().FindAsync(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<TEntity> GetFirst(Expression<Func<TEntity, bool>> searchExpression)
        {
            try
            {
                return await _clientDbContext.Set<TEntity>().Where(searchExpression).FirstOrDefaultAsync();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<TEntity> Insert(TEntity entity)
        {
            try
            {
                entity = _clientDbContext.Set<TEntity>().Add(entity).Entity;
                await _clientDbContext.SaveChangesAsync();

                return entity;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<TEntity> Update(TEntity entity)
        {
            try
            {
                entity = _clientDbContext.Set<TEntity>().Update(entity).Entity;
                await _clientDbContext.SaveChangesAsync();

                return entity;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task Delete(TEntity entity)
        {
            try
            {
                _clientDbContext.Set<TEntity>().Remove(entity);
                await _clientDbContext.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task Delete(int id)
        {
            TEntity entity = await GetById(id);

            if (entity != null)
            {
                await Delete(entity);
            }
        }

        #endregion
    }
}
