﻿using MongoDB.Bson;
using MongoDB.Bson.IO;
using Pragma.Clients.Infrastructure.Interfaces.Data.Utils;
using System.Text.Json;

namespace Pragma.Clients.Infrastructure.Data.Utils
{
    public class BsonDocumentDeserializer<T> : IBsonDocumentDeserializer<T> where T : class
    {
        #region Public Methods

        public T Deserialize(BsonDocument doc)
        {
            if (doc == null)
            {
                return default(T);
            }

            var jsonSettings = new JsonWriterSettings { OutputMode = JsonOutputMode.RelaxedExtendedJson };
            return JsonSerializer.Deserialize<T>(doc.ToJson(jsonSettings));
        }

        #endregion
    }
}
