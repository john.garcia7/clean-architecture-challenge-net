﻿using Pragma.Clients.Core.PocoEntities;

namespace Pragma.Clients.Core.Interfaces
{
    public interface IClientSearchService
    {
        Task<GenericResultsPage<Client>> Search(SearchInfo searchInfo);
    }
}