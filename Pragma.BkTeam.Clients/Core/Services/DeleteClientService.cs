﻿using Pragma.Clients.Core.Exceptions;
using Pragma.Clients.Core.Interfaces;
using Pragma.Clients.Core.PocoEntities;

namespace Pragma.Clients.Core.Services
{
    public class DeleteClientService : IDeleteClientService
    {
        #region Fields

        private readonly IRepository<Client> _repository;

        #endregion

        #region Constructor
        
        public DeleteClientService(
            IRepository<Client> repository)
        {
            _repository = repository;
        }

        #endregion

        #region Public Methods
        
        public async Task Delete(int id)
        {
            bool itemExists = await _repository.GetById(id) != null;

            if (!itemExists)
            {
                throw new ItemNotFoundException(string.Format(Resources.Messages.ItemNotFoundExceptionMsg, id));
            }

            await _repository.Delete(id);
        } 

        #endregion
    }
}
