﻿using Pragma.Clients.Core.Exceptions;
using Pragma.Clients.Core.Interfaces;
using Pragma.Clients.Core.PocoEntities;

namespace Pragma.Clients.Core.Services
{
    public class AddClientService : IAddClientService
    {
        #region Fields

        private readonly IRepository<Client> _repository;

        #endregion

        #region Constructor

        public AddClientService(
            IRepository<Client> repository)
        {
            _repository = repository;
        }

        #endregion

        #region Public Method

        public async Task<Client> Add(Client client)
        {
            bool clientExists = await _repository.GetFirst(dbClient => 
                dbClient.IdType == client.IdType && dbClient.IdNumber == client.IdNumber) != null;

            if (clientExists)
            {
                throw new DuplicatedItemException(
                    string.Format(Resources.Messages.DuplicatedItemExceptionMsg, Enum.GetName(client.IdType), client.IdNumber));
            }

            return await _repository.Insert(client);
        }

        #endregion
    }
}
