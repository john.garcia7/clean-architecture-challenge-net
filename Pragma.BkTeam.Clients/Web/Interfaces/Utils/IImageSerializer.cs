﻿namespace Pragma.Clients.API.Interfaces.Utils
{
    public interface IImageSerializer
    {
        string Serialize(IFormFile image);
    }
}