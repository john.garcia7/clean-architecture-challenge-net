﻿using Pragma.Clients.Infrastructure.Interfaces.Data;

namespace Pragma.Clients.Infrastructure.Data
{
    public class MongoDbSettings : IMongoDbSettings
    {
        #region Properties
        
        public string Database { get; set; }

        public string ConnectionString { get; set; } 

        #endregion
    }
}
