﻿using Pragma.Clients.Core.Exceptions;
using Pragma.Clients.Core.Interfaces;
using Pragma.Clients.Core.PocoEntities;
using System.Linq.Expressions;

namespace Pragma.Clients.Core.Services
{
    public class ClientSearchService : IClientSearchService
    {
        #region Fields

        private readonly IRepository<Client> _repository;
        private readonly IFilterBuilder _filterBuilder;

        #endregion

        #region Constructor

        public ClientSearchService(
            IRepository<Client> repository,
            IFilterBuilder filterBuilder)
        {
            _repository = repository;
            _filterBuilder = filterBuilder;
        }

        #endregion

        #region Public Methods

        public async Task<GenericResultsPage<Client>> Search(SearchInfo searchInfo)
        {
            if (searchInfo.PageInfo == null)
            {
                throw new MandatoryPageInfoException(Resources.Messages.MandatoryPageInfoExceptionMsg);
            }

            Expression<Func<Client, bool>> searchExpression = _filterBuilder.Build(searchInfo.SearchFilter);

            return await _repository.GetAllPaged(searchInfo.PageInfo.Page, searchInfo.PageInfo.PageSize, searchExpression);
        }

        #endregion
    }
}
