﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Pragma.Clients.Core.Exceptions;
using System.Net;

namespace Pragma.Clients.API.ApiFilters
{
    public class CustomExceptionFilter : IExceptionFilter
    {
        #region Events
        
        public void OnException(ExceptionContext context)
        {
            HttpStatusCode httpStatusCode = HttpStatusCode.InternalServerError;

            if (context.Exception.GetType() == typeof(ItemNotFoundException))
            {
                httpStatusCode = HttpStatusCode.NotFound;
            }

            if (context.Exception.GetType() == typeof(MandatoryPageInfoException))
            {
                httpStatusCode = HttpStatusCode.BadRequest;
            }

            ObjectResult response = new ObjectResult(new
            {
                StatusCode = (int)httpStatusCode,
                Message = context.Exception.Message
            })
            {
                StatusCode = (int)httpStatusCode
            };

            context.Result = response;
        } 

        #endregion
    }
}
