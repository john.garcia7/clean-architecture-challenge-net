﻿using Moq;
using Pragma.Clients.Core.Enums;
using Pragma.Clients.Core.Exceptions;
using Pragma.Clients.Core.Interfaces;
using Pragma.Clients.Core.PocoEntities;
using Pragma.Clients.Core.Services;

namespace Pragma.BkTeam.Clients.UnitTests.Core.Services
{
    [TestClass]
    public class GetClientServiceTest
    {
        #region Fields

        private IGetClientService _getClientService;
        private Mock<IRepository<Client>> _repository;

        #endregion

        #region Initialization and Clean Up

        [TestInitialize]
        public void Initialize()
        {
            _repository = new Mock<IRepository<Client>>();
            _getClientService = new GetClientService(_repository.Object);
        }

        #endregion

        #region Test Methods

        [TestMethod]
        public void GetTest()
        {
            #region Arrange
            
            int id = 1;

            Client currentClient = new Client()
            {
                Id = id,
                FirstName = "Arnold",
                LastName = "Spaceman",
                CityOfBirth = "Miami",
                IdType = IdentificationType.DNI,
                IdNumber = "57382838564"
            };

            _repository.Setup(x => x.GetById(id)).Returns(Task.FromResult(currentClient));

            #endregion

            #region Act
            
            Task<Client> client = _getClientService.Get(id);

            #endregion

            #region Assert
            
            Assert.AreEqual(client.Result, currentClient); 

            #endregion
        }

        [TestMethod]
        public async Task GetWithInvalidIdentifierTest()
        {
            #region Arrange
            
            int id = 0;

            #endregion

            #region Act and Assert
            
            await Assert.ThrowsExceptionAsync<InvalidIdentifierException>(() => _getClientService.Get(id)); 

            #endregion
        }

        [TestMethod]
        public async Task GetItemNotFoundTest()
        {
            #region Arrange

            int id = 1000;
            Client existingClient = null;
            _repository.Setup(x => x.GetById(id)).Returns(Task.FromResult(existingClient));

            #endregion

            #region Act and Assert

            await Assert.ThrowsExceptionAsync<ItemNotFoundException>(() => _getClientService.Get(id));

            #endregion
        }

        #endregion
    }
}
