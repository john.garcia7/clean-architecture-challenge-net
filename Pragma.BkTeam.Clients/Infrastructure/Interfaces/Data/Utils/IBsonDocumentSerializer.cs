﻿using MongoDB.Bson;

namespace Pragma.Clients.Infrastructure.Interfaces.Data.Utils
{
    public interface IBsonDocumentSerializer<T> where T : class
    {
        BsonDocument Serialize(T objectToSerialize);
    }
}