using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Pragma.Clients.API.ApiOrchestrators;
using Pragma.Clients.API.Interfaces.ApiOrchestrators;
using Pragma.Clients.API.Interfaces.Mappers;
using Pragma.Clients.API.Interfaces.Utils;
using Pragma.Clients.API.Mappers;
using Pragma.Clients.API.Utils;
using Pragma.Clients.Core.Interfaces;
using Pragma.Clients.Core.PocoEntities;
using Pragma.Clients.Core.Services;
using Pragma.Clients.Core.Utils;
using Pragma.Clients.Infrastructure.Data;
using Pragma.Clients.Infrastructure.Data.Utils;
using Pragma.Clients.Infrastructure.Interfaces.Data;
using Pragma.Clients.Infrastructure.Interfaces.Data.Utils;

string AllowedOrigins = "_allowedOrigins";

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddCors(options =>
{
    options.AddPolicy(name: AllowedOrigins,
        policy =>
        {
            policy.AllowAnyOrigin();
        });
});

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

#region Services Dependency Injection

builder.Services.AddScoped<IBsonDocumentSerializer<Image>, BsonDocumentSerializer<Image>>();
builder.Services.AddScoped<IBsonDocumentDeserializer<Image>, BsonDocumentDeserializer<Image>>();

builder.Services.AddDbContext<ClientDbContext>(options => options.UseMySQL(builder.Configuration.GetConnectionString("ClientDBConnStr")));
builder.Services.Configure<MongoDbSettings>(builder.Configuration.GetSection("MongoDbSettings"));
builder.Services.AddSingleton<IMongoDbSettings>(serviceProvider => serviceProvider.GetRequiredService<IOptions<MongoDbSettings>>().Value);

builder.Services.AddScoped<IImageRepository, ImageRepository>();
builder.Services.AddScoped<IRepository<Client>, GenericRepository<Client>>();

builder.Services.AddScoped<IExpressionCombiner, ExpressionCombiner>();
builder.Services.AddScoped<IFilterBuilder, FilterBuilder>();
builder.Services.AddScoped<IGetClientService, GetClientService>();
builder.Services.AddScoped<IClientSearchService, ClientSearchService>();
builder.Services.AddScoped<IAddClientService, AddClientService>();
builder.Services.AddScoped<IUpdateClientService, UpdateClientService>();
builder.Services.AddScoped<IDeleteClientService, DeleteClientService>();

builder.Services.AddScoped<IImageSerializer, ImageSerializer>();
builder.Services.AddScoped<IClientPageMapper, ClientPageMapper>();
builder.Services.AddScoped<IClientListOrchestrator, ClientListOrchestrator>();
builder.Services.AddScoped<IGetClientOrchestrator, GetClientOrchestrator>();
builder.Services.AddScoped<IAddClientOrchestrator, AddClientOrchestrator>();
builder.Services.AddScoped<IUpdateClientOrchestrator, UpdateClientOrchestrator>();
builder.Services.AddScoped<IDeleteClientOrchestrator, DeleteClientOrchestrator>();

#endregion

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

// Enable CORS
app.UseCors(AllowedOrigins);

app.UseAuthorization();

app.MapControllers();

app.Run();
