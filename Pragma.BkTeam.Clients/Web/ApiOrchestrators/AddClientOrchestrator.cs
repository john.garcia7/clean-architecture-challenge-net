﻿using Pragma.Clients.API.ApiModels;
using Pragma.Clients.API.Interfaces.ApiOrchestrators;
using Pragma.Clients.API.Interfaces.Utils;
using Pragma.Clients.Core.Interfaces;
using Pragma.Clients.Core.PocoEntities;

namespace Pragma.Clients.API.ApiOrchestrators
{
    public class AddClientOrchestrator : IAddClientOrchestrator
    {
        #region Fields

        private readonly IAddClientService _addClientService;
        private readonly IImageRepository _imageRepository;
        private readonly IImageSerializer _imageSerializer;

        #endregion

        #region Constructor

        public AddClientOrchestrator(
            IAddClientService addClientService,
            IImageRepository imageRepository,
            IImageSerializer imageSerializer)
        {
            _addClientService = addClientService;
            _imageRepository = imageRepository;
            _imageSerializer = imageSerializer;
        }

        #endregion

        #region Public Methods

        public async Task<ClientDto> Add(ClientDto client)
        {
            Client savedClient = await _addClientService.Add(client.ToClient());
            _imageRepository.Add(
                new Image 
                { 
                    PersonId = savedClient.Id, 
                    ImageData = _imageSerializer.Serialize(client.Photo) 
                });

            ClientDto clientDto = ClientDto.FromClient(savedClient);
            clientDto.PhotoBase64 = _imageRepository.FindImageByPersonId(savedClient.Id)?.ImageData;

            return clientDto;
        }

        #endregion
    }
}
