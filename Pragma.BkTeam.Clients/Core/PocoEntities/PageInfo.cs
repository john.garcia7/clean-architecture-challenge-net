﻿namespace Pragma.Clients.Core.PocoEntities
{
    public class PageInfo
    {
        #region Properties
        
        public int Page { get; set; }

        public int PageSize { get; set; } 

        #endregion
    }
}
