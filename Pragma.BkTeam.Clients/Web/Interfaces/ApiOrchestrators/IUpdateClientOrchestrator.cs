﻿using Pragma.Clients.API.ApiModels;

namespace Pragma.Clients.API.Interfaces.ApiOrchestrators
{
    public interface IUpdateClientOrchestrator
    {
        Task<ClientDto> Update(int id, ClientDto client);
    }
}