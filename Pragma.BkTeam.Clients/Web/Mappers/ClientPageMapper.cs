﻿using Pragma.Clients.API.ApiModels;
using Pragma.Clients.API.Interfaces.Mappers;
using Pragma.Clients.Core.PocoEntities;

namespace Pragma.Clients.API.Mappers
{
    public class ClientPageMapper : IClientPageMapper
    {
        #region Public Methods
        
        public GenericResultsPage<ClientDto> Map(GenericResultsPage<Client> clientPage)
        {
            return new GenericResultsPage<ClientDto>
            {
                PageInfo = clientPage.PageInfo,
                TotalPages = clientPage.TotalPages,
                TotalResults = clientPage.TotalResults,
                Results = clientPage.Results.Select(ClientDto.FromClient).ToList()
            };
        } 

        #endregion
    }
}
