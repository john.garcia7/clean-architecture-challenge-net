﻿namespace Pragma.Clients.Infrastructure.Interfaces.Data
{
    public interface IMongoDbSettings
    {
        string Database { get; set; }

        string ConnectionString { get; set; }
    }
}
