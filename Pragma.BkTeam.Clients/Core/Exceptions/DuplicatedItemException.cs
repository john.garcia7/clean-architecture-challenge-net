﻿namespace Pragma.Clients.Core.Exceptions
{
    public class DuplicatedItemException : Exception
    {
        #region Constructor
        
        public DuplicatedItemException() { }

        public DuplicatedItemException(string message) : base(message) { }

        public DuplicatedItemException(string message, Exception innerException) : base(message, innerException) { } 

        #endregion
    }
}
