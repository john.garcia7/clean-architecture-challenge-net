﻿namespace Pragma.Clients.Core.PocoEntities
{
    public class SearchFilter
    {
        #region Properties
        
        public string IdType { get; set; }

        public string IdNumber { get; set; }

        public int Age { get; set; } 

        #endregion
    }
}
