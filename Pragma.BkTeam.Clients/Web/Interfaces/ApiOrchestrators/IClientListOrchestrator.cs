﻿using Pragma.Clients.API.ApiModels;
using Pragma.Clients.Core.PocoEntities;

namespace Pragma.Clients.API.Interfaces.ApiOrchestrators
{
    public interface IClientListOrchestrator
    {
        Task<GenericResultsPage<ClientDto>> List(SearchInfo searchInfo);
    }
}