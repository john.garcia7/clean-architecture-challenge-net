﻿using Pragma.Clients.Core.PocoEntities;

namespace Pragma.Clients.Core.Interfaces
{
    public interface IUpdateClientService
    {
        Task<Client> Update(Client client);
    }
}