﻿using Microsoft.AspNetCore.Mvc;
using Pragma.Clients.API.ApiFilters;
using Pragma.Clients.API.ApiModels;
using Pragma.Clients.API.Interfaces.ApiOrchestrators;
using Pragma.Clients.Core.PocoEntities;

namespace Pragma.Clients.API.Controllers
{
    [TypeFilter(typeof(CustomExceptionFilter))]
    [Route("api/client")]
    [ApiController]
    public class ClientController : ControllerBase
    {
        #region Fields
        
        private readonly IGetClientOrchestrator _getClientOrchestrator;
        private readonly IClientListOrchestrator _clientListOrchestrator;
        private readonly IAddClientOrchestrator _addClientOrchestrator;
        private readonly IUpdateClientOrchestrator _updateClientOrchestrator;
        private readonly IDeleteClientOrchestrator _deleteClientOrchestrator;

        #endregion

        #region Constructor

        public ClientController(
            IGetClientOrchestrator getClientOrchestrator,
            IClientListOrchestrator clientListOrchestrator,
            IAddClientOrchestrator addClientOrchestrator,
            IUpdateClientOrchestrator updateClientOrchestrator,
            IDeleteClientOrchestrator deleteClientOrchestrator
            )
        {
            _getClientOrchestrator = getClientOrchestrator;
            _clientListOrchestrator = clientListOrchestrator;
            _addClientOrchestrator = addClientOrchestrator;
            _updateClientOrchestrator = updateClientOrchestrator;
            _deleteClientOrchestrator = deleteClientOrchestrator;
        }

        #endregion

        #region Endpoints
        
        // GET: api/client/5
        [HttpGet("{id}")]
        public async Task<ClientDto> Get(int id)
        {
            return await _getClientOrchestrator.Get(id);
        }

        // POST api/client/list
        [HttpPost("list")]
        public async Task<GenericResultsPage<ClientDto>> List([FromBody] SearchInfo searchInfo)
        {
            return await _clientListOrchestrator.List(searchInfo);
        }

        // POST api/client
        [HttpPost]
        public async Task<ClientDto> Create([FromForm] ClientDto client)
        {
            return await _addClientOrchestrator.Add(client);
        }

        // PUT api/client/5
        [HttpPut("{id}")]
        public async Task<ClientDto> Update(int id, [FromForm] ClientDto client)
        {
            return await _updateClientOrchestrator.Update(id, client);
        }

        // DELETE api/client/5
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await _deleteClientOrchestrator.Delete(id);
        }

        #endregion
    }
}
