﻿namespace Pragma.Clients.Core.Exceptions
{
    public class InvalidIdentificationTypeException : Exception
    {
        #region Constructor
        
        public InvalidIdentificationTypeException() { }

        public InvalidIdentificationTypeException(string message) : base(message) { }

        public InvalidIdentificationTypeException(string message, Exception innerException) : base(message, innerException) { } 

        #endregion
    }
}
