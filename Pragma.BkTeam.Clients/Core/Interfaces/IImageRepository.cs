﻿using Pragma.Clients.Core.PocoEntities;

namespace Pragma.Clients.Core.Interfaces
{
    public interface IImageRepository
    {
        Dictionary<int, string> FindImagesByPeopleIdList(List<int> peopleIdList);

        Image FindImageByPersonId(int personId);

        Image Add(Image image);

        void DeleteImageByPersonId(int personId);

        void Update(Image image);
    }
}
