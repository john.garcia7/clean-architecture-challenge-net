﻿using Pragma.Clients.API.Interfaces.Utils;

namespace Pragma.Clients.API.Utils
{
    public class ImageSerializer : IImageSerializer
    {
        #region Public Methods

        public string Serialize(IFormFile image)
        {
            string serializedImage = null;

            using (MemoryStream stream = new MemoryStream())
            {
                image.CopyTo(stream);
                byte[] fileBytes = stream.ToArray();
                serializedImage = Convert.ToBase64String(fileBytes);
            }

            return serializedImage;
        }

        #endregion
    }
}
