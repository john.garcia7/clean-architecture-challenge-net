﻿using Moq;
using Pragma.Clients.Core.Enums;
using Pragma.Clients.Core.Exceptions;
using Pragma.Clients.Core.Interfaces;
using Pragma.Clients.Core.PocoEntities;
using Pragma.Clients.Core.Services;
using System.Linq.Expressions;

namespace Pragma.BkTeam.Clients.UnitTests.Core.Services
{
    [TestClass]
    public class UpdateClientServiceTest
    {
        #region Fields

        private IUpdateClientService _updateClientService;
        private Mock<IRepository<Client>> _repository;

        #endregion

        #region Initialization and Clean Up

        [TestInitialize]
        public void Initialize()
        {
            _repository = new Mock<IRepository<Client>>();
            _updateClientService = new UpdateClientService(_repository.Object);
        }

        #endregion

        #region Test Methods

        [TestMethod]
        public void UpdateClientTest()
        {
            #region Arrange

            Client client = new Client()
            {
                Id = 1,
                FirstName = "Sharon",
                LastName = "Stone",
                CityOfBirth = "New York",
                IdType = IdentificationType.DNI,
                IdNumber = "648374623832"
            };

            _repository.Setup(x => x.GetById(1)).Returns(Task.FromResult(client));

            #endregion

            #region Act

            _updateClientService.Update(client);

            #endregion

            #region Assert

            _repository.Verify(x => x.Update(client), Times.Once());

            #endregion
        }

        [TestMethod]
        public async Task DuplicatedClientTest()
        {
            #region Arrange

            Client existingClient = new Client()
            {
                Id = 1,
                FirstName = "Sharon",
                LastName = "Stone",
                CityOfBirth = "New York",
                IdType = IdentificationType.DNI,
                IdNumber = "648374623832"
            };

            Client updatedClient = new Client()
            {
                Id = 2,
                FirstName = "Demi",
                LastName = "Moore",
                CityOfBirth = "New York",
                IdType = IdentificationType.DNI,
                IdNumber = "648374623832"
            };

            _repository.Setup(x => x.GetById(updatedClient.Id)).Returns(Task.FromResult(existingClient));
            _repository.Setup(x =>
                x.GetFirst(It.IsAny<Expression<Func<Client, bool>>>())).Returns(Task.FromResult(existingClient));

            #endregion

            #region Act and Assert

            await Assert.ThrowsExceptionAsync<DuplicatedItemException>(() => _updateClientService.Update(updatedClient));
            _repository.Verify(x => x.Update(updatedClient), Times.Never());

            #endregion
        }

        [TestMethod]
        public async Task GetItemNotFoundTest()
        {
            #region Arrange

            Client updatedClient = new Client()
            {
                Id = 1000,
                FirstName = "Sandra",
                LastName = "Bullock",
                CityOfBirth = "Washington",
                IdType = IdentificationType.CC,
                IdNumber = "9386375492"
            };

            Client existingClient = null;
            _repository.Setup(x => x.GetById(updatedClient.Id)).Returns(Task.FromResult(existingClient));

            #endregion

            #region Act and Assert

            await Assert.ThrowsExceptionAsync<ItemNotFoundException>(() => _updateClientService.Update(updatedClient));
            _repository.Verify(x => x.Update(updatedClient), Times.Never());

            #endregion
        }

        #endregion
    }
}
