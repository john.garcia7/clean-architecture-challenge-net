﻿using Pragma.Clients.Core.Exceptions;
using Pragma.Clients.Core.Interfaces;
using Pragma.Clients.Core.PocoEntities;

namespace Pragma.Clients.Core.Services
{
    public class GetClientService : IGetClientService
    {
        #region Fields

        private readonly IRepository<Client> _repository;

        #endregion

        #region Constructor

        public GetClientService(
            IRepository<Client> repository)
        {
            _repository = repository;
        }

        #endregion

        #region Public Methods

        public async Task<Client> Get(int id)
        {
            if (id < 1)
            {
                throw new InvalidIdentifierException(Resources.Messages.InvalidIdentifierExceptionMsg);
            }

            Client client = await _repository.GetById(id);

            if(client == null)
            {
                throw new ItemNotFoundException(string.Format(Resources.Messages.ItemNotFoundExceptionMsg, id));
            }

            return client;
        }

        #endregion
    }
}
