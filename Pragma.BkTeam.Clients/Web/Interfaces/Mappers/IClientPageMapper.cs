﻿using Pragma.Clients.API.ApiModels;
using Pragma.Clients.Core.PocoEntities;

namespace Pragma.Clients.API.Interfaces.Mappers
{
    public interface IClientPageMapper
    {
        GenericResultsPage<ClientDto> Map(GenericResultsPage<Client> clientPage);
    }
}