﻿using Moq;
using Pragma.Clients.Core.Exceptions;
using Pragma.Clients.Core.Interfaces;
using Pragma.Clients.Core.PocoEntities;
using Pragma.Clients.Core.Services;
using System.Linq.Expressions;

namespace Pragma.BkTeam.Clients.UnitTests.Core.Services
{
    [TestClass]
    public class ClientSearchServiceTest
    {
        #region Fields

        private IClientSearchService _clientSearchService;        
        private Mock<IRepository<Client>> _repository;
        private Mock<IFilterBuilder> _filterBuilder;

        #endregion

        #region Initialization and Clean Up

        [TestInitialize]
        public void Initialize()
        {
            _repository = new Mock<IRepository<Client>>();
            _filterBuilder = new Mock<IFilterBuilder>();
            _clientSearchService = new ClientSearchService(_repository.Object, _filterBuilder.Object);
        }

        #endregion

        #region Test Methods

        [TestMethod]
        public void GetAllTest()
        {
            #region Arrange

            SearchInfo searchInfo = new SearchInfo()
            {
                PageInfo = new PageInfo()
                {
                    Page = 1,
                    PageSize = 10
                }
            };

            GenericResultsPage<Client> expectedClientsPage = new GenericResultsPage<Client>
            {
                PageInfo = new PageInfo { Page = 1, PageSize = 10 },
                Results = new List<Client>() { new Client(), new Client() },
                TotalPages = 1,
                TotalResults = 2
            };

            _repository.Setup(x => x.GetAllPaged(searchInfo.PageInfo.Page, searchInfo.PageInfo.PageSize, null)).Returns(Task.FromResult(expectedClientsPage));

            #endregion

            #region Act
            
            Task<GenericResultsPage<Client>> clientsPage = _clientSearchService.Search(searchInfo);

            #endregion

            #region Assert
            
            Assert.AreEqual(expectedClientsPage, clientsPage.Result); 

            #endregion
        }

        [TestMethod]
        public async Task MandatoryPageInfoTest()
        {
            #region Arrange
            
            SearchInfo searchInfo = new SearchInfo() { PageInfo = null };

            #endregion

            #region Act and Assert
            
            await Assert.ThrowsExceptionAsync<MandatoryPageInfoException>(() => _clientSearchService.Search(searchInfo));
            _repository.Verify(x => x.GetAllPaged(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<Expression<Func<Client, bool>>>()), Times.Never());

            #endregion
        }

        #endregion
    }
}
