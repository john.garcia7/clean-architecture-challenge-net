﻿using Pragma.Clients.Core.PocoEntities;
using System.Linq.Expressions;

namespace Pragma.Clients.Core.Interfaces
{
    public interface IRepository<TEntity> where TEntity : class
    {        
        Task<List<TEntity>> GetAll(Expression<Func<TEntity, bool>> searchExpression = null);

        Task<GenericResultsPage<TEntity>> GetAllPaged(int page, int pageSize, Expression<Func<TEntity, bool>> searchExpression = null);

        Task<TEntity> GetById(int id);

        Task<TEntity> GetFirst(Expression<Func<TEntity, bool>> searchExpression);

        Task<TEntity> Insert(TEntity entity);

        Task<TEntity> Update(TEntity entity);

        Task Delete(TEntity entity);

        Task Delete(int id);
    }
}
