﻿using Pragma.Clients.API.Interfaces.ApiOrchestrators;
using Pragma.Clients.Core.Interfaces;

namespace Pragma.Clients.API.ApiOrchestrators
{
    public class DeleteClientOrchestrator : IDeleteClientOrchestrator
    {
        #region Fields

        private readonly IDeleteClientService _deleteClientService;
        private readonly IImageRepository _imageRepository;

        #endregion

        #region Constructor

        public DeleteClientOrchestrator(
            IDeleteClientService deleteClientService,
            IImageRepository imageRepository)
        {
            _deleteClientService = deleteClientService;
            _imageRepository = imageRepository;
        }

        #endregion

        #region Public Methods

        public async Task Delete(int id)
        {
            await _deleteClientService.Delete(id);
            _imageRepository.DeleteImageByPersonId(id);
        }

        #endregion
    }
}
