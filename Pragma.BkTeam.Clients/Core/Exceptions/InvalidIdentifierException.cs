﻿namespace Pragma.Clients.Core.Exceptions
{
    public class InvalidIdentifierException : Exception
    {
        #region Constructor
        
        public InvalidIdentifierException() { }

        public InvalidIdentifierException(string message) : base(message) { }

        public InvalidIdentifierException(string message, Exception innerException) : base(message, innerException) { } 

        #endregion
    }
}
