﻿using Pragma.Clients.Core.PocoEntities;
using System.Linq.Expressions;

namespace Pragma.Clients.Core.Interfaces
{
    public interface IFilterBuilder
    {
        Expression<Func<Client, bool>> Build(SearchFilter searchFilter);
    }
}