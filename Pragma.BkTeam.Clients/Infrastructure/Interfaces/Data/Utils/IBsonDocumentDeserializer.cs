﻿using MongoDB.Bson;

namespace Pragma.Clients.Infrastructure.Interfaces.Data.Utils
{
    public interface IBsonDocumentDeserializer<T> where T : class
    {
        T Deserialize(BsonDocument doc);
    }
}