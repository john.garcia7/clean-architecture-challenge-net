﻿using Moq;
using Pragma.Clients.Core.Exceptions;
using Pragma.Clients.Core.Interfaces;
using Pragma.Clients.Core.PocoEntities;
using Pragma.Clients.Core.Services;

namespace Pragma.BkTeam.Clients.UnitTests.Core.Services
{
    [TestClass]
    public class DeleteClientServiceTest
    {
        #region Fields

        private IDeleteClientService _deleteClientService;
        private Mock<IRepository<Client>> _repository;

        #endregion

        #region Initialization and Clean Up

        [TestInitialize]
        public void Initialize()
        {
            _repository = new Mock<IRepository<Client>>();
            _deleteClientService = new DeleteClientService(_repository.Object);
        }

        #endregion

        #region Test Methods

        [TestMethod]
        public void DeleteTest()
        {
            #region Arrange
            
            int idToDelete = 5;
            Client currentClient = new Client() { Id = idToDelete };

            _repository.Setup(x => x.GetById(idToDelete)).Returns(Task.FromResult(currentClient));

            #endregion

            #region Act

            _deleteClientService.Delete(idToDelete);

            #endregion

            #region Assert
            
            _repository.Verify(x => x.Delete(idToDelete), Times.Once()); 

            #endregion
        }

        [TestMethod]
        public async Task ItemToDeleteNotFoundTest()
        {
            #region Arrange

            int idToDelete = 5;

            #endregion

            #region Act and Assert

            await Assert.ThrowsExceptionAsync<ItemNotFoundException>(() => _deleteClientService.Delete(idToDelete));
            _repository.Verify(x => x.GetById(idToDelete), Times.Once());
            _repository.Verify(x => x.Delete(idToDelete), Times.Never());

            #endregion
        }

        #endregion
    }
}
