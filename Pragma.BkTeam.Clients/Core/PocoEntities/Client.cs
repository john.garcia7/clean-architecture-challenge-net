﻿using Pragma.Clients.Core.Enums;

namespace Pragma.Clients.Core.PocoEntities
{
    public class Client
    {
        #region Properties
        
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public IdentificationType IdType { get; set; }

        public string IdNumber { get; set; }

        public int Age { get; set; }

        public string CityOfBirth { get; set; } 

        #endregion
    }
}
