# Clean Architecture Challenge .NET



## Getting started

- Clone or download the repository contents and open the **Pragma.BkTeam.Clients.sln** file located on the **Pragma.BkTeam.Clients** folder to explore the .NET solution created using Clean Architecture (Onion).

- Restore the databases backups included in the **Databases BKs** folder (A MySQL database and a MongoDB collection).

- Set the Pragma.Clients.Api as the startup project and run it using Visual Studio.

- You can run the unit tests with Visual Studio and you can access the code coverage currently generated in the path Pragma.BkTeam.Clients\Pragma.BkTeam.Clients.UnitTests\coveragereport\index.htm. 


## Code coverage

The coverage report is not generated automatically when you run unit tests in Visual Studio, you need to generate manually using the coverlet commands. For more information see: https://github.com/coverlet-coverage/coverlet

The coverage was configured to evaluate only the classes contained in **Pragma.Clients.Core.Services** namespace that contains the business rules.


## How to test the API functionality

Test the API using Postman with the following url endpoints:

Get a client: **GET** https://localhost:7091/api/client/{client-id}<br>
Create a client: **POST** https://localhost:7091/api/client<br>
List/Search clients: **POST** https://localhost:7091/api/client/list<br>
Update a client: **PUT** https://localhost:7091/api/client/{client-id}<br>
Delete a client: **DELETE** https://localhost:7091/api/client/{client-id}<br>

For more information about the use and parameters of the services, please run the application with Visual Studio and it automatically open the **OpenAPI (Swagger)** documentation using the following url: https://localhost:7091/swagger/index.html
