﻿using MongoDB.Bson;
using MongoDB.Driver;
using Pragma.Clients.Core.Interfaces;
using Pragma.Clients.Core.PocoEntities;
using Pragma.Clients.Infrastructure.Interfaces.Data;
using Pragma.Clients.Infrastructure.Interfaces.Data.Utils;

namespace Pragma.Clients.Infrastructure.Data
{
    public class ImageRepository : IImageRepository
    {
        #region Constants

        private const string ImagesCollection = "images";
        private const string PersonId = "PersonId";
        private const string ImageData = "ImageData";        

        #endregion

        #region Fields

        private readonly IMongoCollection<BsonDocument> _collection;
        private readonly IBsonDocumentSerializer<Image> _bsonDocumentSerializer;
        private readonly IBsonDocumentDeserializer<Image> _bsonDocumentDeserializer;

        #endregion

        #region Constructor
        
        public ImageRepository(
            IMongoDbSettings mongoDbSettings, 
            IBsonDocumentSerializer<Image> bsonDocumentSerializer,
            IBsonDocumentDeserializer<Image> bsonDocumentDeserializer)
        {            
            MongoClient _mongoClient = new MongoClient(mongoDbSettings.ConnectionString);
            IMongoDatabase _database = _mongoClient.GetDatabase(mongoDbSettings.Database);
            _collection = _database.GetCollection<BsonDocument>(ImagesCollection);

            _bsonDocumentSerializer = bsonDocumentSerializer;
            _bsonDocumentDeserializer = bsonDocumentDeserializer;
        }

        #endregion

        #region Public Methods

        public void DeleteImageByPersonId(int personId)
        {
            var filter = Builders<BsonDocument>.Filter.Eq(PersonId, personId);

            _collection.DeleteOne(filter);
        }

        public Image FindImageByPersonId(int personId)
        {
            var filter = Builders<BsonDocument>.Filter.Eq(PersonId, personId);

            return _bsonDocumentDeserializer.Deserialize(_collection.Find(filter).FirstOrDefault());
        }

        public Dictionary<int, string> FindImagesByPeopleIdList(List<int> peopleIdList)
        {
            var filter = Builders<BsonDocument>.Filter.In(PersonId, peopleIdList);

            return _collection.Find(filter).ToList().Select(_bsonDocumentDeserializer.Deserialize)
                .ToDictionary(image => image.PersonId, image => image.ImageData);
        }

        public Image Add(Image image)
        {
            _collection.InsertOne(_bsonDocumentSerializer.Serialize(image));

            return FindImageByPersonId(image.PersonId);
        }

        public void Update(Image image)
        {
            var filter = Builders<BsonDocument>.Filter.Eq(PersonId, image.PersonId);
            var update = Builders<BsonDocument>.Update.Set(ImageData, image.ImageData);

            _collection.UpdateOne(filter, update);
        }

        #endregion
    }
}
