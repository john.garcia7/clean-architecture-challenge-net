﻿using Pragma.Clients.API.ApiModels;
using Pragma.Clients.API.Interfaces.ApiOrchestrators;
using Pragma.Clients.Core.Interfaces;

namespace Pragma.Clients.API.ApiOrchestrators
{
    public class GetClientOrchestrator : IGetClientOrchestrator
    {
        #region Fields

        private readonly IGetClientService _getClientService;
        private readonly IImageRepository _imageRepository;

        #endregion

        #region Constructor

        public GetClientOrchestrator(
            IGetClientService getClientService, 
            IImageRepository imageRepository)
        {
            _getClientService = getClientService;
            _imageRepository = imageRepository;
        }

        #endregion

        #region Public Methods

        public async Task<ClientDto> Get(int id)
        {
            ClientDto client = ClientDto.FromClient(await _getClientService.Get(id));
            client.PhotoBase64 = _imageRepository.FindImageByPersonId(id).ImageData;

            return client;
        }

        #endregion
    }
}
