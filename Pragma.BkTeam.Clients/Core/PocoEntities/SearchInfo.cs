﻿namespace Pragma.Clients.Core.PocoEntities
{
    public class SearchInfo
    {
        #region Properties
        
        public SearchFilter SearchFilter { get; set; }

        public PageInfo PageInfo { get; set; } = new PageInfo();

        #endregion
    }
}
