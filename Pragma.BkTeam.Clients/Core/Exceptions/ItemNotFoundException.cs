﻿namespace Pragma.Clients.Core.Exceptions
{
    public class ItemNotFoundException : Exception
    {
        #region Constructor
        
        public ItemNotFoundException() { }

        public ItemNotFoundException(string message) : base(message) { }

        public ItemNotFoundException(string message, Exception innerException) : base(message, innerException) { } 

        #endregion
    }
}
