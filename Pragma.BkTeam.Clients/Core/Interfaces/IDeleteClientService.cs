﻿namespace Pragma.Clients.Core.Interfaces
{
    public interface IDeleteClientService
    {
        Task Delete(int id);
    }
}