﻿namespace Pragma.Clients.Core.PocoEntities
{
    public class Image
    {
        #region Public Properties
        
        public int PersonId { get; set; }

        public string ImageData { get; set; } 

        #endregion
    }
}
