﻿using Pragma.Clients.API.ApiModels;
using Pragma.Clients.API.Interfaces.ApiOrchestrators;
using Pragma.Clients.API.Interfaces.Utils;
using Pragma.Clients.Core.Interfaces;
using Pragma.Clients.Core.PocoEntities;

namespace Pragma.Clients.API.ApiOrchestrators
{
    public class UpdateClientOrchestrator : IUpdateClientOrchestrator
    {
        #region Fields

        private readonly IUpdateClientService _updateClientService;
        private readonly IImageRepository _imageRepository;
        private readonly IImageSerializer _imageSerializer;

        #endregion

        #region Constructor

        public UpdateClientOrchestrator(
            IUpdateClientService updateClientService,
            IImageRepository imageRepository,
            IImageSerializer imageSerializer)
        {
            _updateClientService = updateClientService;
            _imageRepository = imageRepository;
            _imageSerializer = imageSerializer;
        }

        #endregion

        #region Public Methods

        public async Task<ClientDto> Update(int id, ClientDto client)
        {
            client.Id = id;

            Client savedClient = await _updateClientService.Update(client.ToClient());
            _imageRepository.Update(
                new Image 
                { 
                    PersonId = client.Id, 
                    ImageData = _imageSerializer.Serialize(client.Photo) 
                });

            ClientDto clientDto = ClientDto.FromClient(savedClient);
            clientDto.PhotoBase64 = _imageRepository.FindImageByPersonId(savedClient.Id)?.ImageData;

            return ClientDto.FromClient(savedClient);
        }

        #endregion
    }
}
