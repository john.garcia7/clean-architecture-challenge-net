﻿using Pragma.Clients.Core.Enums;
using Pragma.Clients.Core.PocoEntities;
using System.ComponentModel.DataAnnotations;

namespace Pragma.Clients.API.ApiModels
{
    public class ClientDto
    {
        #region Properties
        
        public int Id { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string IdType { get; set; }

        [Required]
        public string IdNumber { get; set; }

        public int Age { get; set; }

        public string CityOfBirth { get; set; }

        public IFormFile Photo { get; set; }

        public string PhotoBase64 { get; set; }

        #endregion

        #region Model Mappers

        public static ClientDto FromClient(Client client)
        {
            return new ClientDto
            {
                Id = client.Id,
                Age = client.Age,
                CityOfBirth = client.CityOfBirth,
                FirstName = client.FirstName,
                LastName = client.LastName,
                IdType = Enum.GetName(typeof(IdentificationType), client.IdType),
                IdNumber = client.IdNumber
            };
        }

        public Client ToClient()
        {
            return new Client
            {
                Id = Id,
                Age = Age,
                CityOfBirth = CityOfBirth,
                FirstName = FirstName,
                LastName = LastName,
                IdType = (IdentificationType)Enum.Parse(typeof(IdentificationType), IdType),
                IdNumber = IdNumber
            };
        } 

        #endregion
    }
}
