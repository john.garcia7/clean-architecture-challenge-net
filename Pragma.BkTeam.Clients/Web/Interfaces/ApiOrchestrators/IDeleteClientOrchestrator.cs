﻿namespace Pragma.Clients.API.Interfaces.ApiOrchestrators
{
    public interface IDeleteClientOrchestrator
    {
        Task Delete(int id);
    }
}