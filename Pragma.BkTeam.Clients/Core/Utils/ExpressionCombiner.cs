﻿using System.Linq.Expressions;
using Pragma.Clients.Core.Extensions;
using Pragma.Clients.Core.Interfaces;

namespace Pragma.Clients.Core.Utils
{
    public class ExpressionCombiner : IExpressionCombiner
    {
        #region Public Methods
        
        public Expression<Func<T, bool>> Combine<T>(Expression<Func<T, bool>> combinedExpression, Expression<Func<T, bool>> expressionToConcat)
        {
            if (expressionToConcat == null)
            {
                return combinedExpression;
            }

            if (combinedExpression == null)
            {
                combinedExpression = expressionToConcat;
            }
            else
            {
                if(expressionToConcat != null)
                {
                    combinedExpression = combinedExpression.And(expressionToConcat);
                }                
            }

            return combinedExpression;
        } 

        #endregion
    }
}
