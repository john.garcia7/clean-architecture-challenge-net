﻿namespace Pragma.Clients.Core.Exceptions
{
    public class MandatoryPageInfoException : Exception
    {
        #region Constructor
        
        public MandatoryPageInfoException() { }

        public MandatoryPageInfoException(string message) : base(message) { }

        public MandatoryPageInfoException(string message, Exception innerException) : base(message, innerException) { } 

        #endregion
    }
}
