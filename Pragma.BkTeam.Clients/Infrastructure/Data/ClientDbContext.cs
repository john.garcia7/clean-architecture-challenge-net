﻿using Microsoft.EntityFrameworkCore;
using Pragma.Clients.Core.PocoEntities;

namespace Pragma.Clients.Infrastructure.Data
{
    public class ClientDbContext : DbContext
    {
        #region Constructor

        public ClientDbContext(DbContextOptions<ClientDbContext> options) : base(options) 
        { 
            ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }

        #endregion

        #region DbSets

        public DbSet<Client> Clients { get; set; }

        #endregion

        #region Overrides
        
        protected override void OnModelCreating(ModelBuilder modelBuilder) { } 

        #endregion
    }
}
