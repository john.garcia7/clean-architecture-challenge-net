﻿using Pragma.Clients.API.ApiModels;
using Pragma.Clients.API.Interfaces.ApiOrchestrators;
using Pragma.Clients.API.Interfaces.Mappers;
using Pragma.Clients.Core.Interfaces;
using Pragma.Clients.Core.PocoEntities;

namespace Pragma.Clients.API.ApiOrchestrators
{
    public class ClientListOrchestrator : IClientListOrchestrator
    {
        #region Fields

        private readonly IClientSearchService _clientSearchService;
        private readonly IClientPageMapper _clientPageMapper;
        private readonly IImageRepository _imageRepository;

        #endregion

        #region Constructor

        public ClientListOrchestrator(
            IClientSearchService clientSearchService,
            IClientPageMapper clientPageMapper,
            IImageRepository imageRepository)
        {
            _clientSearchService = clientSearchService;
            _clientPageMapper = clientPageMapper;
            _imageRepository = imageRepository;
        }

        #endregion

        #region Public Methods

        public async Task<GenericResultsPage<ClientDto>> List(SearchInfo searchInfo)
        {
            GenericResultsPage<ClientDto> clientsPage = _clientPageMapper.Map(await _clientSearchService.Search(searchInfo));

            Dictionary<int, string> clientImages = _imageRepository.FindImagesByPeopleIdList(
                clientsPage.Results.Select(client => client.Id).ToList());

            clientsPage.Results.ForEach(client => client.PhotoBase64 = clientImages[client.Id]);

            return clientsPage;
        }

        #endregion
    }
}
