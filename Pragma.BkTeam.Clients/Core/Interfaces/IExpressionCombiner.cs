﻿using System.Linq.Expressions;

namespace Pragma.Clients.Core.Interfaces
{
    public interface IExpressionCombiner
    {
        Expression<Func<T, bool>> Combine<T>(Expression<Func<T, bool>> combinedExpression, Expression<Func<T, bool>> expressionToConcat);
    }
}